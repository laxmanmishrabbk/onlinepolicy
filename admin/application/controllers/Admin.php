<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('common_model');
    }
    public function index(){
		$this->load->view('login');
	}
	public function login()
	{
		if($this->input->post()) {
			$authenticated = $this->common_model->select_fields_where('admin','*',array('email'=>$this->input->post('email'),'password'=>$this->input->post('password')));
			if(!$authenticated) {
				$this->session->set_flashdata('error', 'Invalid login!!!');
				redirect(base_url());
			} else {
				redirect('admin/dashboard');
			}
		}
		$this->load->view('login');
	}
	
	public function logout() 
	{
		unset($_SESSION['userinfo']);
		unset($_SESSION['logged_in']);
		$this->session->set_flashdata('notice', 'You have been logged out successfully!!!');
		redirect('admin');
	}

	public function check_login(){
		$rowst = array();

		redirect('dashboard');
	}
	public function dashboard(){
		$this->load->view('index');
	}

}